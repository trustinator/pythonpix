#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='PythonPix',
    version="0.1.0",
    description='Convertion tools in Python',
    author='gregory STOTE',
    url='https://gitlab.com/trustinator/pythonpix.git',
    packages=find_packages(),
    include_package_data=True, 
    install_requires=['Pillow'],
    entry_points={
        'console_scripts': [
            'PythonPix = PythonPix.core:main',
        ],
    }
)