#!/usr/bin/env python3

from PIL import Image
from glob import glob

import os

import logging

logger = logging.getLogger(__name__)

#################################################################
def convert_bw(dir_name=None, images=None, layer_list=None):
    """
        Convert pictures in black and white

        :param str dirname : directory name
        :param str images : list of file name
        :param list layer_list : id of the layer
    """

    logging.basicConfig(level=logging.INFO)
    logging.getLogger("PIL").setLevel(logging.NOTSET)

    for i in layer_list:
        if i in ("r","g","b"):
            try:
                for image in images:
                    img = Image.open(image)
                    if i == "r":
                        rgb = "red"
                        layer_id = 0
                    elif i == "g":
                        rgb = "green"
                        layer_id = 1
                    elif i == "b":
                        rgb = "blue"
                        layer_id = 2

                    filename, ext = os.path.splitext(image)

                    tmp_img = img.split()[layer_id]

                    tmp_img.save(f"{dir_name}/{filename}-BW-{rgb}{ext}")

                    logging.info("Convertion of layer "f"{rgb} of the picture "f"{image} in black and white is done !")
                    
            except Exception as err:
                logging.info("Convertion of layer "f"{rgb} in black and white error !")
                logging.info(str(err))

        if i == "l":
            try:
                for image in images:
                    img = Image.open(image)

                    filename, ext = os.path.splitext(image)

                    l = img.convert('L')

                    l.save(f"{dir_name}/{filename}-BW-Lum{ext}")
                    
                    logging.info("Convertion of layer luminescence of the picture "f"{image} in black and white is done !")

            except Exception as err:
                logging.info("Convertion error !")
                logging.info(str(err))


#################################################################
# Main
def main_convert_bw(dir_name=None, images=None, layer=None):
    """
        Main of convert in black and white

        :param str dirname : directory name
        :param str images : list of file name
        :param int layer_id : id of the layer
    """
    
    convert_bw(dir_name, images, layer)
    logging.info("Process finish, picture available in = "f"{dir_name} directory !")