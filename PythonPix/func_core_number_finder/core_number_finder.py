import logging

import multiprocessing


logger = logging.getLogger(__name__)



#################################################################
def get_core_number():

    logging.basicConfig(level=logging.INFO)

    # Create jpg list
    core_number = multiprocessing.cpu_count()

    return core_number



#################################################################
# Main
def main_core_number_finder():
    
    core_number = get_core_number()
    return core_number