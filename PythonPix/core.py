#!/usr/bin/env python3
#################################################################
# Script for generate black and white picture from a jpg
# Layer r = Red
# Layer g = Green
# Layer b = Blue
# Layer l = Luminescence
#################################################################

from PIL import Image
# from glob import glob
import math


import os
import argparse
import logging

import threading


from .func_bw.convert_bw import main_convert_bw
from .func_dir.create_dir import main_create_dir
from .func_gen_list_pix.gen_list_pix import main_gen_list_pix
from .func_gen_dict_list_pix.gen_dict_list_pix import main_gen_dict_list_pix
from .func_core_number_finder.core_number_finder import main_core_number_finder

logger = logging.getLogger(__name__)

#################################################################
# Main, take first argument (file name) for launch the process

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--out_dir", "-d",   
                dest = "out_dir",
                help = "Directory name for output images, it will be create by the script, some privileges can be mandatory in some directory !",
                default = "BW",
                type = str,
                required = False)

    parser.add_argument("--layer", "-l",   
                dest = "layer",
                choices=["r", "g", "b", "l"],
                help = "Layer ID : r = Red, g = Green, b = Blue, l = Luminescence(Default), 0 2 = Red + Blue, 0 1 2 3 = All",
                default = "l",
                type = str,
                nargs = "+",
                required = False)
    
    # Variables set
    args = parser.parse_args()
    dir_name = args.out_dir
    layer_input_list = args.layer
    args = parser.parse_args()


    # Get core number
    core_number = main_core_number_finder()
    logging.info("Thread detected : "f"{core_number} !")

    # Create directory
    main_create_dir(dir_name)

    # Generate list of pictures
    list_pix = main_gen_list_pix()

    # Generate dict of list of pictures for core dispatching
    dict_list_pix = main_gen_dict_list_pix(list_pix, core_number)

    
    division_factor = math.ceil(len(list_pix) / core_number)
    n_list = math.ceil(len(list_pix) / division_factor)
    logging.info("Thread used : "f"{n_list} !")


    # If you use default value for the layer, this variable is int, it become list here
    layer_list = []
    if isinstance(layer_input_list, int):
        layer_list.append(layer_input_list)
    else:
        layer_list = layer_input_list
   
    # Launch the process
    for i in range(0, n_list):
        t = threading.Thread(target=main_convert_bw, args=(dir_name, dict_list_pix[i], layer_list))
        t.start()


#################################################################
# Main
if __name__ == "__main__":
    main()