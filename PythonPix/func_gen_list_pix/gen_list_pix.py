import logging

from glob import glob

logger = logging.getLogger(__name__)



#################################################################
def gen_list():

    logging.basicConfig(level=logging.INFO)

    # Create jpg list
    images_list = glob("*.jpg")
    return images_list



#################################################################
# Main
def main_gen_list_pix():
  
    images_list = gen_list()
    return images_list