import logging
import math


logger = logging.getLogger(__name__)




#################################################################

def gen_dict_list(list_pix=None, core_number=None):

    """
        gen_dict_list

        :param list list_pix : list of images
        :param list core_number : number of core
    """


    begin_images = 0
    division_factor = math.ceil(len(list_pix) / core_number)
    end_images = division_factor
    n_list = math.ceil(len(list_pix) / division_factor)

    dict_list_pix = {}


    try:
        for i in range(n_list):
                try:
                    if end_images > len(list_pix) - 1:
                        end_images = len(list_pix)
                        dict_list_pix[i] = list_pix[begin_images:end_images]
                        
                    elif begin_images > len(list_pix) - 1:
                        begin_images = len(list_pix) - 1
                        dict_list_pix[i] = list_pix[begin_images:end_images]
                    else:
                        dict_list_pix[i] = list_pix[begin_images:end_images]
                        begin_images += division_factor
                        end_images += division_factor
                except Exception as err:
                    logging.info("Dispatch images error !")
                    logging.info(str(err))

    except Exception as err:
        logging.info("Generate dict of list error !")

    finally:
        return dict_list_pix


#################################################################
# Main
def main_gen_dict_list_pix(list_pix=None, core_number=None):
    """
        Main of gen_list_pix

        :param list list_pix : list of images
        :param list core_number : number of core

    """
    
    dict_list_pix = gen_dict_list(list_pix, core_number)
    return dict_list_pix