import os
import logging

logger = logging.getLogger(__name__)

#################################################################
def create_directory(dir_name=None):
    """
        Create directory for receive the black and white picture

        :param str dirname : directory name
    """

    logging.basicConfig(level=logging.INFO)
    
    try:
        # Create target Directory
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
            logging.info("Directory "f"{dir_name} created !")

    except Exception as err:
        logging.info("Directory "f"{dir_name} error !")


#################################################################
# Main
def main_create_dir(dir_name=None):
    """
        Main of create directory

        :param str dirname : directory name
    """
    
    create_directory(dir_name)