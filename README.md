# Script for convert jpg picture in black and white
Script for generate black and white picture from all the jpg files present inside a directory.

This python script support multithreading for a maximum of four threads, one layer = one thread.

Layer r = Generate black and white picture from red layer.

Layer g = Generate black and white picture from green layer.

Layer b = Generate black and white picture from blue layer.

Layer l = Generate black and white picture from luminescence layer.


## Prerequisite :
Python 3.6.7

pip3

Pillow library



## How to launch the script :
Launch your favorite shell
Place the module PythonPix.py inside the picture directory.
From the picture directory launch the module like this :

For the help :
```shell
python3 -m PythonPix.core --help
```

For generate black and white picture from the default layer (luminescence) inside the "BW" directory of your picture directory :
```shell
python3 -m PythonPix.core 
```

For generate black and white picture from all layers inside the "/tmp/BW" directory of your computer :
```shell
python3 -m PythonPix.core --out_dir /tmp/BW --layer r g b l
```

For generate black and white picture from the layer red and Luminescence inside the "temp" directory of thei python script :
```shell
python3 -m PythonPix.core --out_dir ./temp --layer r l
```

This script is delivered with 5 free pictures of New York for launch some tests.


# Done

## 01/24/2019
* Dispatch works accross all cores available on the computer.
* Choose where the pictures can be saved (outside the picture directory).


## 01/10/2019 :
* Convert in python module
* Move function outside principal code
* Replace print by logging

## 12/31/2018 : 
* Replace layer 0 1 2 and 4 by r g b and l.
* Change the name to PythonPix
* Some modifications.

## 26/31/2018 :
* Initial release.

# To do for the next version
* Choose the source picture directory (outside the python directory).
* Add support of others pictures format (png, bmp, etc....).
* Add convertion format feature.
* Add reduce size feature.
* Facial recognition for automatic indexation.
* Posibility to convert just few picture (input the picture name to the lauch command).
* Add a config file for set some parameters :
    - Outside directory
    - Favorite layers